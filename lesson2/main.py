a = [1, 2, 4]
b = a.copy()
a[0] = 12

print(b)
# ------------------------------------

a, *b, c = (1, 2, 3, 4, 5, 7)

print(a)
print(b)
print(c)


# ------------------------------------
lst = [1, 2, 3, 4, 5]


def for_map(x):
    return x + 1


a = map(lambda x: x+1, lst)
b = map(for_map, lst)
c = (x+1 for x in lst)


def some(a=[]):
    a.append(1)
    print(a)


if __name__ == '__main__':
    print(some.__defaults__)
    some()  # [1]
    some()  # [1, 1]
    some()  # [1, 1, 1]


# class MyClass:
#
#     def __enter__(self):
#         print("Enter")
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print("Exit")
#
#
# with MyClass() as new_file:
#     new_file.write("Hello world")
#
# # new_file = open("filename.txt", 'w')
# # new_file.write("Hello world")
# # new_file.close()
#
#
# class MyHeshable:
#
#     def __hash__(self):
#         return 1
#
#
# my_dct = {
#     MyHeshable(): "test",
#     MyHeshable(): "test2"
# }
